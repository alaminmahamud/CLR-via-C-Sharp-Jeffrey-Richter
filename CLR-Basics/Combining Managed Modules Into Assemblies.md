The CLR doesn’t actually work with modules, it works with assemblies. An assembly is an abstract
concept that can be difficult to grasp initially. First, an assembly is a logical grouping of one or more
modules or resource files. Second, an assembly is the smallest unit of reuse, security, and versioning.
Depending on the choices you make with your compilers or tools, you can produce a single-file or a
multifile assembly. In the CLR world, an assembly is what we would call a component.


![](http://imgur.com/yFu9bym)
